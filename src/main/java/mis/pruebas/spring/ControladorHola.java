package mis.pruebas.spring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControladorHola {
    @GetMapping("/hola")
    public String hola(){
        return "Hola";
    }
}
